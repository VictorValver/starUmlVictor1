package Model;


import java.util.*;

/**
 * 
 */
public class Persona {

    /**
     * Default constructor
     */
    public Persona() {
    }

    /**
     * 
     */
    public int id;

    /**
     * 
     */
    public String nombre;

    /**
     * 
     */
    public String apellidos;

    /**
     * 
     */
    public String direccion;

    /**
     * 
     */
    public String ciudad;

    /**
     * 
     */
    public int cp;

    /**
     * 
     */
    public String provincia;

    /**
     * 
     */
    public String pais;

    /**
     * 
     */
    public String fechaNacimiento;

    /**
     * 
     */
    public String equipo;

}